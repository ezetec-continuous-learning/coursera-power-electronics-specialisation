% Homework Assignment #3: Ch. 3 - Equivalent Circuit Modeling

% Initialise script
clc; clear all; close all;


% ===================================================================== PART A

% Parameters
V = 5;                   % Output voltage [V]
I = 1;                   % Output current [I]
Vg = 1.5;                % Input battery voltage [V]
fs = 40e3;               % Switching frequency [Hz]
Ts = 1/fs;               % Switching period [s]
L = 100e-6;              % Inductance [H]
Vd = 0.4;                % Diode foreward voltage
Ron = 8e-3;              % MOSFET on-resistance

% Early values for evaluations
D = 0.5;           
RL = 1e-6;
R = V/I;

% 1 -------------------------------------------------------------------------- 
% Expression for the effective turns ratio m1
m1 = "D";
disp(["1: " m1]);
eval(["m1 = " m1 ";"])

% 2 -------------------------------------------------------------------------- 
% Expression for Re
Re = "RL + D*Ron";
disp(["2: " Re]);
eval(["Re = " Re ";"])

% 3 -------------------------------------------------------------------------- 
% Expression for Ve
Ve = "(1-D)*Vd";
disp(["3: " Ve]);
eval(["Ve = " Ve ";"]);

% 4 -------------------------------------------------------------------------- 
% Expression for the effective turns ratio m2
m2 = "1/(1-D)";
disp(["4: " m2]);
eval(["m2 = " m2 ";"]);

% 5 -------------------------------------------------------------------------- 
% Expression for the conversion ratio V / Vg
VonVgStr = "(D/(1-D)) * ( 1 - ((1-D)*Vd/D/Vg)) / ((D*Ron/R/(1-D)^2) + (RL/R/(1-D)^2) +1 )";
disp(["5: " VonVgStr]);
eval(["VonVgStr = " VonVgStr ";"]);

% 6 -------------------------------------------------------------------------- 
% Expression for the converter efficiency
eta = "( 1 - ((1-D)*Vd/D/Vg)) / ((D*Ron/R/(1-D)^2) + (RL/R/(1-D)^2) +1 )";
disp(["6: " eta]);
eval(["eta = " eta ";"]);

% 7 -------------------------------------------------------------------------- 
% Inductor resistance numerical value
eta = 0.85;
RL = "(eta * Vg)^2 * (V/eta -Vd - V) / I / (eta*Vg + V)^2 - Ron*V/(eta*Vg +V)";
eval(["RL = " RL ";"]);
disp(["7: " num2str(RL)]);

% 8 -------------------------------------------------------------------------- 
% Duty cyle numerical value
D = "V / (eta * Vg + V)";
eval(["D = " D ";"]);
disp(["8: " num2str(D)]);

% ===================================================================== PART A

% Parameters
Vbatt = 240;              % Battery supply voltage [V]
Vbus = 500;               % AC motor converter bus [V]
VT = 1;                   % IGBT on-state voltage [V]
RT = 1.6e-3;              % IGBT on-state resistance [Ohm]
Vd = 1.2;                 % Diode foreward voltage [V]
RL = 4e-3;                % Inductor resistance [Ohm]
Pbus = 150e3;             % Power input to AC motor converter bus [W]

% 9 -------------------------------------------------------------------------- 
% Express Re
disp(["9: " "RL + D * RT"]);

% 10 ------------------------------------------------------------------------- 
% Express Ve
disp(["10: " "D * VT + (1-D) * Vd"]);

% 11 ------------------------------------------------------------------------- 
% Express m
disp(["11: " "1/(1-D)"]);

% 12 ------------------------------------------------------------------------- 
% Calculate the duty cycle
a = VT*Vbus - Vd*Vbus - Vbus^2;
b = - Vbatt*Vbus - RT*Pbus - VT*Vbus + 2*Vd*Vbus + 2*Vbus^2;
c = Vbatt*Vbus - RL*Pbus - Vd*Vbus - Vbus^2;
D = (-b + [1 -1].*sqrt(b^2 - 4*a*c))/(2*a); D = D(D > 0);
D = min(D);
disp(["12: " num2str(D)]);

% 13 ------------------------------------------------------------------------- 
% Converter efficiency
eta = Vbus * (1-D) / Vbatt;
disp(["13: " num2str(eta)]);

% 14 ------------------------------------------------------------------------- 
% Conduction loss of the IGBT
PT = D * RT * Pbus^2/(1-D)^2/Vbus^2;
disp(["14: " num2str(PT)]);
