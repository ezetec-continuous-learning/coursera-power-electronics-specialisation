% Homework Assignment #2: Ch. 2 - Converter Analysis

% Initialise script
clc; clear all; close all;

% Parameters
Vg = 12;          % Converter input voltage [V]
V  = -5;          % Converter output voltage [V]
R  = 2.5;         % Load impedance [Ohms]
fs = 200 * 1e3;   % Switching frequency [kHz]
Ts = 1/fs;

% 1 -------------------------------------------------------------------------- 
% Derive an expression for the dc component of the inductor voltage
disp(["1: " "Vg * D + V * (1-D)"]);

% 2 -------------------------------------------------------------------------- 
% Derive an expression for the dc component of the capacitor current
disp(["2: " "- V / R - IL * (1-D)"]);

% 3 -------------------------------------------------------------------------- 
% Derive an expression for the dc output voltage V

% The volt-second balance property is applied:                                    
disp(["3: " "- Vg * D / (1 - D)"]);

% 4 -------------------------------------------------------------------------- 
% Derive an expression for the dc component of the inductor current IL
disp(["4: " "Vg * D / (R * (1 - D)^2)"]);

% 5 -------------------------------------------------------------------------- 
% Derive an expression for the inductor peak current ripple
disp(["5: " "Vg * D * Ts / (2 * L)"]);


% 6 -------------------------------------------------------------------------- 
% Derive an expression for the capacitor peak voltage ripple 
disp(["6: " "Vg * Ts * D^2 / ( 2 * C * R * (1 - D))"]);

% 7 -------------------------------------------------------------------------- 
% Compute the numerical value of the duty cycle D
D = V / (V - Vg);
disp(["7: " num2str(D)]);

% 8 -------------------------------------------------------------------------- 
% Compute the numerical value of the dc component of the inductor current, 
% in amperes. 
I_L = Vg * D / (R * (1 - D)^2);
disp(["8: " num2str(I_L)]);

% 9 -------------------------------------------------------------------------- 
% Calculate the value of the inductance L 
Delta_i_L = abs(0.15 * I_L);
L = 1e6 * Vg * D * Ts / ( 2 * Delta_i_L);
disp(["9: " num2str(L)]);

% 10 ------------------------------------------------------------------------- 
% Compute the value of capacitance C 
Delta_v_C = 25e-3;
C = 1e6 * Vg * D^2 * Ts / (2 * Delta_v_C * R * (1-D));
disp(["10: " num2str(C)]);

% 11 ------------------------------------------------------------------------- 
% Find the MOSFET peak current value in amperes
i_Q1_peak = I_L + Delta_i_L;
disp(["11: " num2str(i_Q1_peak)]);

 
% 12 ------------------------------------------------------------------------- 
% Compute the MOSFET peak voltage value in volts
v_Q1_peak = Vg - V + Delta_v_C;
disp(["12: " num2str(v_Q1_peak)]);

% 13 ------------------------------------------------------------------------- 
% Compute the MOSFET DC voltage component
V_Q1 = (1 - D)*(Vg - V);
disp(["13: " num2str(V_Q1)]);

% 14 ------------------------------------------------------------------------- 
% Expression for the dc component of the output voltage V
disp(["14: " "Vg * D"]);

% 15 ------------------------------------------------------------------------- 
% Expression for the dc component of the L2
disp(["15: " "Vg * D / R"]);

% 16 ------------------------------------------------------------------------- 
% Expression for the dc component of the capacitor C1
disp(["16: " "Vg"]);

% 17 ------------------------------------------------------------------------- 
% Expression for the dc component of the inductor L1
disp(["17: " "Vg * D^2 / R"]);

% 18 ------------------------------------------------------------------------- 
% Expression for the capacitor C1C_1C1​ peak-to-average voltage ripple
disp(["18: " "Vg * (1-D) * D^2 * Ts / (2 * R * C1)"]);

% 19 ------------------------------------------------------------------------- 
% Expression for the inductor L1L_1L1​ peak-to-average current ripple
disp(["19: " "Vg * (1-D) * D^2 * Ts^2 / (16 * R * L1 * C1)"]);